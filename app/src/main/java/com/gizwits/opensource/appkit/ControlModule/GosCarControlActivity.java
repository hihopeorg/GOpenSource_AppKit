package com.gizwits.opensource.appkit.ControlModule;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gizwits.opensource.appkit.CommonModule.GosBaseActivity;
import com.gizwits.opensource.appkit.R;
import com.hisilicion.histreaming.GizWifiDevice;
import com.hisilicion.histreaming.GizWifiSDK;

public class GosCarControlActivity extends GosBaseActivity {
    private Button m_goForwardButton;
    private Button m_turnLeftButton;
    private Button m_StopButton;
    private Button m_turnRightButton;
    private Button m_turnBackButton;
    private Button m_Going;
    private Button m_Backing;
    private GizWifiDevice device;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gos_car_control);
        initDevice();
        setActionBar(true,true,"CarControl");
        initView();
    }

    private void initView() {
        m_goForwardButton = (Button)findViewById(R.id.go_forward_bt);
        m_turnLeftButton = (Button)findViewById(R.id.turn_left_bt);
        m_StopButton = (Button)findViewById(R.id.stop_bt);
        m_turnRightButton = (Button)findViewById(R.id.turn_right_bt);
        m_turnBackButton = (Button)findViewById(R.id.turn_back_bt);
        m_Going = (Button)findViewById(R.id.going_bt);
        m_Backing = (Button)findViewById(R.id.backing_bt);

        m_goForwardButton.setOnClickListener(new ButtonListener());
        m_turnLeftButton.setOnClickListener(new ButtonListener());
        m_StopButton.setOnClickListener(new ButtonListener());
        m_turnRightButton.setOnClickListener(new ButtonListener());
        m_turnBackButton.setOnClickListener(new ButtonListener());
        m_Going.setOnClickListener(new ButtonListener());
        m_Backing.setOnClickListener(new ButtonListener());

        m_goForwardButton.setOnLongClickListener(new ButtonListener());
        m_turnBackButton.setOnLongClickListener(new ButtonListener());
        m_turnLeftButton.setOnLongClickListener(new ButtonListener());
        m_turnRightButton.setOnLongClickListener(new ButtonListener());
    }


    private void initDevice() {
        Intent intent = getIntent();
        device = (GizWifiDevice) intent.getParcelableExtra("GizWifiDevice");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ButtonListener implements View.OnClickListener, View.OnLongClickListener {

        @SuppressLint("WrongConstant")
        @Override
        public void onClick(View view) {
            int code = -2;
            switch (view.getId()){
                case R.id.going_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","going");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "go forward", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.backing_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","backing");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "go forward", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.go_forward_bt: //前进
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","forward");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "go forward", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_left_bt:  //向左
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","left");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "turn_left", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.stop_bt:       //暂停
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","stop");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "stop", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_right_bt: //向右
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","right");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "turn_right", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_back_bt:  //向后
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","back");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "turn_back", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                default:
                    break;
            }
        }

        @Override
        public boolean onLongClick(View view) {
            int code = -2;
            switch (view.getId()){
                case R.id.go_forward_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","going");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "go forward", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_back_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","backing");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "go forward", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_left_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","lefting");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "turn_left_bt", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                case R.id.turn_right_bt:
                    code = GizWifiSDK.sharedInstance().post(device.m_index,"CarControl","righting");
                    if(code == 0){
                        Toast.makeText(GosCarControlActivity.this, "turn_right_bt", 100).show();//add by wgm
                    }else if(code == -2){
                        Toast.makeText(GosCarControlActivity.this, device.alias + " is Offline", 2000).show();
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    }
}
