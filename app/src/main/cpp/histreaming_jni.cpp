/*----------------------------------------------------------------------------
* Copyright (c) <2013-2015>, <Huawei Technologies Co., Ltd>
* All rights reserved.
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
* 1. Redistributions of source code must retain the above copyright notice, this list of
* conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list
* of conditions and the following disclaimer in the documentation and/or other materials
* provided with the distribution.
* 3. Neither the name of the copyright holder nor the names of its contributors may be used
* to endorse or promote products derived from this software without specific prior written
* permission.
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*---------------------------------------------------------------------------*/

#include <jni.h>
#include <stdio.h>
#include <link_platform.h>
#include <link_service_agent.h>
#include <link_agent.h>
#include <status_code.h>
#include <string>
#include "utils_log.h"
#include "jni_utils.h"

#define JNI_CLASS_HISTREAMING_LINKPLATFORM     "com/hisilicion/histreaming/LinkPlatform"
#define JNI_CLASS_HISTREAMING_QUERY_RESULT     "com/hisilicion/histreaming/QueryResult"
#define JNI_CLASS_HISTREAMING_SERVICE_AGENT    "com/hisilicion/histreaming/LinkServiceAgent"

#ifndef NELEM
#define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))
#endif

struct fields_t {
    jfieldID coreContext;
    jfieldID qresContext;
    jfieldID agentContext;
};
struct fields_t g_context;

static void jstringTostring(JNIEnv *env, jstring jstr, std::string &value)
{
    const char *str;
    str = env->GetStringUTFChars(jstr, JNI_FALSE);
    if (str == NULL) {
        LOGW("Get content from JAVA string failure\n");
        return;
    }

    value = str;
    (*env).ReleaseStringUTFChars(jstr, str);

    LOGI("JString to CString=%s\n", value.data());

    return;
}

class LinkCore
{
public:
    LinkCore()
        : m_linkPlatform{nullptr},
          m_linkAgent{nullptr}
    {
    };

    ~LinkCore()
    {
        if (m_linkPlatform) {
            LinkPlatformFree(m_linkPlatform);
        }

        if (m_linkAgent) {
            LinkAgentFree(m_linkAgent);
        }
    }

    int open()
    {
        m_linkPlatform = LinkPlatformGet();
        if (m_linkPlatform == nullptr) {
            return StatusFailure;
        }

        m_linkAgent = LinkAgentGet(m_linkPlatform);
        if (m_linkAgent == nullptr) {
            return StatusFailure;
        }

        if (m_linkPlatform->open(m_linkPlatform) != StatusOk) {
            return StatusFailure;
        }

        return StatusOk;
    }

    int close()
    {
        if (m_linkPlatform) {
            m_linkPlatform->close(m_linkPlatform);
        }

        return StatusOk;
    }

    int discover(std::string &type) {
        if (m_linkPlatform) {
            return m_linkPlatform->discover(m_linkPlatform, type.data());
        } else {
            return StatusFailure;
        }

        return StatusOk;
    }

    QueryResult *query(std::string &type)
    {
        if (m_linkAgent && m_linkPlatform) {
            return m_linkAgent->query(m_linkAgent, type.data());
        } else {
            return NULL;
        }
    }

    struct LinkPlatform *m_linkPlatform;
    struct LinkAgent *m_linkAgent;
};

static LinkCore *get_core_context_from_java(JNIEnv *env, jobject obj)
{
    return (LinkCore *) env->GetLongField(obj, g_context.coreContext);
}

static QueryResult *get_qres_context_from_jave(JNIEnv *env, jobject obj)
{
    return (QueryResult *) env->GetLongField(obj, g_context.qresContext);
}

static LinkServiceAgent *get_agent_context_from_jave(JNIEnv *env, jobject obj)
{
    return (LinkServiceAgent *) env->GetLongField(obj, g_context.agentContext);
}

static jint OpenLinkCore(JNIEnv *env, jobject thiz)
{
    auto core = get_core_context_from_java(env, thiz);
    if (core == nullptr) {
        core = new LinkCore();
        if (core == nullptr) {
            return StatusFailure;
        } else {
            env->SetLongField(thiz, g_context.coreContext, (jlong) core);
        }

        if (core->open() != StatusOk) {
            return StatusFailure;
        }
    } else {
        LOGW("LinkCore had craeted\n");
        return StatusFailure;
    }

    return StatusOk;
}

static jint CloseLinkCore(JNIEnv *env, jobject thiz)
{
    auto core = get_core_context_from_java(env, thiz);
    if (core != nullptr) {
        core->close();
        env->SetLongField(thiz, g_context.coreContext, (jlong) 0);
        delete core;
    }

    return StatusOk;
}

static jint DiscoverDevices(JNIEnv *env, jobject thiz, jstring type)
{
    std::string value;
    jstringTostring(env, type, value);
    LOGI("type = %s\n", value.data());

    auto core = get_core_context_from_java(env, thiz);
    if (core == nullptr) {
        return StatusFailure;
    }

    core->discover(value);

    return StatusOk;
}

static jobject QueryDevices(JNIEnv *env, jobject thiz, jstring type)
{
    std::string value;
    jstringTostring(env, type, value);
    LOGI("type = %s\n", value.data());

    auto core = get_core_context_from_java(env, thiz);
    if (core == nullptr) {
        return nullptr;
    }

    QueryResult* qres = core->query(value);
    if (qres == nullptr) {
        return nullptr;
    }

    jclass myClass = env->FindClass(JNI_CLASS_HISTREAMING_QUERY_RESULT);    
    jmethodID id = env->GetMethodID(myClass, "<init>", "()V");
    jobject qresJava = env->NewObject(myClass, id);
    if (qresJava != nullptr) {
        env->SetLongField(qresJava, g_context.qresContext, (jlong) qres);
    } else {
        QueryResultFree(qres);
    }

    return qresJava;
}


static jint QueryResultCount(JNIEnv *env, jobject thiz)
{
    auto q = get_qres_context_from_jave(env, thiz);
    if (q != nullptr) {
        return q->count(q);
    }

    return 0;
}

static jobject QueryResultAt(JNIEnv *env, jobject thiz, jint index)
{
    auto q = get_qres_context_from_jave(env, thiz);
    if (q == nullptr) {
        return nullptr;
    }

    LinkServiceAgent *a = q->at(q, index);
    if (a == nullptr) {
        return nullptr;
    }

    jclass myClass = env->FindClass(JNI_CLASS_HISTREAMING_SERVICE_AGENT);    
    jmethodID id = env->GetMethodID(myClass, "<init>", "()V");
    jobject oJava = env->NewObject(myClass, id);
    if (oJava != nullptr) {
        env->SetLongField(oJava, g_context.agentContext, (jlong) a);
    } else {
        LOGD("QueryResultAt ~~~~~` null");
    }

    return oJava;
}

static void FreeQueryResult(JNIEnv *env, jobject thiz)
{
    auto q = get_qres_context_from_jave(env, thiz);
    if (q == nullptr) {
        return;
    }

    env->SetLongField(thiz, g_context.qresContext, (jlong) 0);
    QueryResultFree(q);
    return;
}


static jstring AgentType(JNIEnv *env, jobject thiz)
{
    auto a = get_agent_context_from_jave(env, thiz);
    if (a == nullptr) {
        LOGD("AgentType ~~~~~` null");
        return nullptr;
    }

    return env->NewStringUTF(a->type(a));
}
static jstring AgentAddress(JNIEnv *env, jobject thiz)
{
    auto a = get_agent_context_from_jave(env, thiz);
    if (a == nullptr) {
        LOGD("AgentAddress ~~~~~` null");
        return nullptr;
    }

    return env->NewStringUTF(a->addr(a));
}
static jint AgentId(JNIEnv *env, jobject thiz)
{
    auto a = get_agent_context_from_jave(env, thiz);
    if (a == nullptr) {
        return 0;
    }

    return a->id(a);
}
static jstring AgentGet(JNIEnv *env, jobject thiz, jstring property)
{
    auto a = get_agent_context_from_jave(env, thiz);
    if (a == nullptr) {
        return nullptr;
    }

    std::string value;
    jstringTostring(env, property, value);
    LOGI("get type = %s\n", value.data());

    char buff[0xff] = {'\0'};
    int code = a->get(a, value.data(), buff, 0xff);
    if (code == StatusFailure) {
        return nullptr;
    }

    return env->NewStringUTF(buff);
}
static jint AgentModify(JNIEnv *env, jobject thiz, jstring property, jstring v)
{
    auto a = get_agent_context_from_jave(env, thiz);
    if (a == nullptr) {
        return StatusFailure;
    }

    std::string prop;
    std::string value;
    jstringTostring(env, property, prop);
    jstringTostring(env, v, value);
    LOGI("modify type = %s(%s)\n", prop.data(), value.data());

    int code = a->modify(a, prop.data(), (char*)(value.data()), value.length());
    if (code == StatusFailure) {
        return StatusFailure;
    } else if (code == StatusDeviceOffline) {
        return -2;
    }

    return StatusOk;
}

static JNINativeMethod g_methods_linkcore[] = {
    {"open",     "()I",                   (void *) OpenLinkCore},
    {"close",    "()I",                   (void *) CloseLinkCore},
    {"discover", "(Ljava/lang/String;)I", (void *) DiscoverDevices},
    {"query",    "(Ljava/lang/String;)Lcom/hisilicion/histreaming/QueryResult;", (void *) QueryDevices},
};

static JNINativeMethod g_methods_query[] = {
    {"count",     "()I",                   (void *) QueryResultCount},
    {"at",    "(I)Lcom/hisilicion/histreaming/LinkServiceAgent;",     (void *) QueryResultAt},
    {"free",    "()V",     (void *) FreeQueryResult},
};

static JNINativeMethod g_methods_agent[] = {
    {"type",     "()Ljava/lang/String;",                        (void *) AgentType},
    {"address",    "()Ljava/lang/String;",                      (void *) AgentAddress},
    {"id",    "()I",                                            (void *) AgentId},
    {"get",    "(Ljava/lang/String;)Ljava/lang/String;",        (void *) AgentGet},
    {"modify",    "(Ljava/lang/String;Ljava/lang/String;)I",   (void *) AgentModify},

};

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    JNIEnv *env;
    if (JNI_OK != vm->GetEnv(reinterpret_cast<void **> (&env), JNI_VERSION_1_4)) {
        LOGW("JNI_OnLoad could not get JNI env");
        return JNI_ERR;
    }

    jclass clazz = env->FindClass(JNI_CLASS_HISTREAMING_LINKPLATFORM);
    if (env->RegisterNatives(clazz, g_methods_linkcore, sizeof(g_methods_linkcore) / sizeof((g_methods_linkcore)[0])) < 0) {
        LOGW("RegisterNatives error");
        return JNI_ERR;
    }
    g_context.coreContext = env->GetFieldID(clazz, "m_context", "J");
    if (nullptr == g_context.coreContext) {
        LOGE("can't find field m_context.");
        return 0;
    }

    clazz = env->FindClass(JNI_CLASS_HISTREAMING_QUERY_RESULT);
    if (env->RegisterNatives(clazz, g_methods_query, sizeof(g_methods_query) / sizeof((g_methods_query)[0])) < 0) {
        LOGW("RegisterNatives error");
        return JNI_ERR;
    }
    g_context.qresContext = env->GetFieldID(clazz, "m_context", "J");
    if (nullptr == g_context.qresContext) {
        LOGE("can't find field m_context.");
        return JNI_ERR;
    }

    clazz = env->FindClass(JNI_CLASS_HISTREAMING_SERVICE_AGENT);
    if (env->RegisterNatives(clazz, g_methods_agent, sizeof(g_methods_agent) / sizeof((g_methods_agent)[0])) < 0) {
        LOGW("RegisterNatives error");
        return JNI_ERR;
    }
    g_context.agentContext = env->GetFieldID(clazz, "m_context", "J");
    if (nullptr == g_context.agentContext) {
        LOGE("can't find field m_context.");
        return JNI_ERR;
    }

    LOGI("JNI_OnLoad....");

    return JNI_VERSION_1_4;
}

JNIEXPORT void JNI_OnUnload(JavaVM *jvm, void *reserved)
{
    LOGI("JNI_OnUnload....");
}

